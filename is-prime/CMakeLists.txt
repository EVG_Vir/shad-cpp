cmake_minimum_required(VERSION 2.8)
project(is-prime)

include(../common.cmake)

set(SRCS is_prime.cpp)

if (ENABLE_PRIVATE_TESTS)

endif()

if (TEST_SOLUTION)
    set(SRCS ../private/is-prime/is_prime_baseline.cpp)
endif()

add_library(is_prime ${SRCS})

add_gtest(test_isprime test.cpp)
target_link_libraries(test_isprime is_prime)

add_benchmark(bench_isprime run.cpp)
target_link_libraries(bench_isprime is_prime)
